import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import model.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Konrad on 02.10.2016:10:04
 */
public class MainVerticle extends AbstractVerticle {
    //5-issue
    private static final String DATA_COLLECTION = "Data-Collection";
    private JDBCClient jdbc;

    @Override
    public void start(Future<Void> fut) {
        startBackend(
                (connection) -> initDataBase(connection,
                        (nothing) -> startWebApp(
                                (http) -> completeStartup(http, fut)
                        ), fut
                ), fut);
    }

    private void completeStartup(AsyncResult<HttpServer> http, Future<Void> fut) {
        if (http.succeeded()) {
            fut.complete();
        } else {
            fut.fail(http.cause());
        }
    }

    private void startBackend(Handler<AsyncResult<SQLConnection>> next, Future<Void> fut) {
        config().put("url", "jdbc:hsqldb:mem:test?shutdown=true");
        config().put("driver_class", "org.hsqldb.jdbcDriver");
        jdbc = JDBCClient.createShared(vertx, config(), DATA_COLLECTION);

        jdbc.getConnection(ar -> {
            if (ar.failed()) {
                fut.fail(ar.cause());
            } else {
                next.handle(Future.succeededFuture(ar.result()));
            }
        });
    }

    private void initDataBase(AsyncResult<SQLConnection> result,
                                Handler<AsyncResult<Void>> next, Future<Void> fut) {


        if (result.failed()) {
            fut.fail(result.cause());
        } else {
            SQLConnection connection = result.result();
            connection.execute(
                    "CREATE TABLE IF NOT EXISTS Data (id INTEGER IDENTITY, data varchar(1000))",
                    ar -> {
                        if (ar.failed()) {
                            fut.fail(ar.cause());
                            connection.close();
                            return;
                        }
                        connection.query("SELECT * FROM Data", select -> {
                            if (select.failed()) {
                                fut.fail(ar.cause());
                                connection.close();
                            } else {
                                next.handle(Future.<Void>succeededFuture());
                                connection.close();
                            }
                        });
                    });
        }
    }//fixme

    private void startWebApp(Handler<AsyncResult<HttpServer>> next) {
        // Create a router object.
        Router router = Router.router(vertx);


        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response.putHeader("content-type", "text/html")
                    .end("vertx-performance-test works");
        });

        router.route("/ok").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response.putHeader("content-type", "text/html")
                    .end("ok");
        });


        router.route("/response").handler(BodyHandler.create());//this receives body of the request
        router.route("/response").handler(routingContext -> {
            final Data data = Json.decodeValue(routingContext.getBodyAsString(), Data.class);
            routingContext.response()
                    .setStatusCode(200)
                    .putHeader("content-type", "text/html; charset=utf-8")
                    .end(data.data);
        });


        router.route("/data").handler(BodyHandler.create());//this receives body of the request
        router.get("/data").handler(this::getAll);
        router.post("/data").handler(this::postData);
        router.put("/data").handler(this::putData);


        // Create the HTTP server and pass the "accept" method to the request handler.
        vertx
                .createHttpServer()
                .requestHandler(router::accept)
                .listen(
                        // Retrieve the port from the configuration,
                        // default to 8080.
                        config().getInteger("http.port", 8080),
                        next
                );
    }

    private void postData(RoutingContext routingContext) {
        final Data data = Json.decodeValue(routingContext.getBodyAsString(), Data.class);

        jdbc.getConnection(ar -> {
            SQLConnection connection = ar.result();
            String sql = "INSERT INTO Data (data) VALUES ?";
            connection.updateWithParams(sql,
                    new JsonArray().add(data.data),
                    (ardat) -> {
                        if (ar.failed()) {
                            return;
                        }
                        UpdateResult result = ardat.result();
                        Data w = new Data(result.getKeys().getLong(0), data.data);
                        routingContext.response()
                                .setStatusCode(201)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(Json.encodePrettily(w));
                    });
        });
    }

    private void putData(RoutingContext routingContext) {
        final Data data = Json.decodeValue(routingContext.getBodyAsString(), Data.class);

        if (data.id == null) {
            routingContext.response().setStatusCode(404).end("No id");
        }
        jdbc.getConnection(ar -> {
            SQLConnection connection = ar.result();
            String sql = "UPDATE Data SET data = ? WHERE id = ?";
            connection.updateWithParams(sql,
                    new JsonArray().add(data.data).add(data.id),
                    (ardat) -> {
                        if (ar.failed() || ardat.result() == null) {
                            routingContext.response().setStatusCode(404).end();
                            return;
                        }
                        routingContext.response()
                                .setStatusCode(201)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(Json.encodePrettily(data));
                    });
        });
    }

    private void getAll(RoutingContext routingContext) {
        jdbc.getConnection(ar -> {
            SQLConnection connection = ar.result();
            connection.query("SELECT * FROM Data", result -> {
                List<Data> dataList = result.result().getRows().stream().map(Data::new).collect(Collectors.toList());
                routingContext.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(dataList));
                connection.close(); // Close the connection here some changes
            });
        });
    }

}
