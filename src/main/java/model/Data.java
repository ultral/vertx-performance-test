package model;

import io.vertx.core.json.JsonObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Data {

    @Id
    @GeneratedValue
    public Long id;

    @NotNull
    public String data;

    public Data(JsonObject json) {
        this.data = json.getString("DATA");
        this.id = json.getLong("ID");
    }

    public Data(){
        this.id = null;
        this.data = "";
    }

    public Data(Long id, String data) {
        this.data = data;
        this.id = id;
    }

}
